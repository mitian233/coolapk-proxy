from flask import Flask, request, send_file
import asyncio
import json, requests, io

app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False  # json出力のソートを無効化

# CORS対策
def after_request(resp):
    resp.headers['Access-Control-Allow-Origin'] = '*'
    resp.headers['Access-Control-Allow-Headers'] = 'Content-Type,Authorization'
    resp.headers['Access-Control-Allow-Methods'] = 'GET,POST,PUT,DELETE,OPTIONS'
    return resp

app.after_request(after_request)

ua = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36 Edg/109.0.1518.70"


@app.route('/')
def hello_world():  # put application's code here
    returnData = json.loads('{"message": "Hello World"}')
    if request.args.get('name'):
        returnData['message'] = "Hello " + request.args.get('name')
    if request.args.get('coolapkpic'):
        if (request.args.get('coolapkpic').find("coolapk.com") == -1):
            returnData = json.loads('{"message": "Error"}')
        else:
            try:
                Buffer = io.BytesIO()
                r = requests.get(request.args.get('coolapkpic'), headers={"User-Agent": ua}, verify=False)
                filename = request.args.get('coolapkpic').split('/')[-1]
                Buffer.write(r.content)
                Buffer.seek(0)
                returnData = send_file(Buffer, mimetype='image', attachment_filename=filename, as_attachment=False)
            except:
                returnData = json.loads('{"message": "Error"}')
    return returnData

@app.route('/status')
def return_status():
    r = requests.get("https://www.coolapk1s.com/status", headers={"User-Agent": ua}, verify=False)
    rDump = json.loads(r.content)
    return rDump


@app.route('/feed/id/<id>')
def return_feed(id):
    r = requests.get("https://coolapk-api-go.vercel.app/api/feed/detail?id=" + id, headers={"User-Agent": ua},
                     verify=False)
    rDump = json.loads(r.content)
    return rDump


if __name__ == '__main__':
    app.run()
